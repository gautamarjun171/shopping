<?php

namespace App\Http\Controllers\Admin;

use App\User;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Category;
use Auth;

class AdminController extends Controller
{

    public function getDashboard() {
        return view('AdminLte.dashboard');
    }

    public function getLogin() {
        if(Auth::check() === true){
            return view('AdminLte.dashboard');
        }
        return view('AdminLte.Auth.login');
    }

    public function getProduct() {
        $maincat = Category::where('parentId','=','0')->get();
        return view('AdminLte.pages.product.product',['maincat'=> $maincat]);
    }
      
    public function getCategory(){
        return view('AdminLte.pages.category.product');
    }
}