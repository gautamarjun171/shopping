<?php

namespace App\Http\Controllers\Admin\Auth;

use App\User;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{

    // public function __construct(){
    //     $this->middleware('auth');
    // }
    
    
    public function postLogin(Request $request) {

        if ((Auth::attempt(['email' => $request->email, 'password' => $request->password]))){

    //        dd((Auth::user()));

                if(Auth::user()->IsAdmin == 1) {
                    return redirect()->intended('admin/dashboard');
                } else {
                    return redirect()->intended('/');
                }
            }
            // if unsuccessful -> redirect back
                return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors([
                'approve' => 'Wrong credentials or this account not approved yet.',
           ]);

        }

        public function logout(Request $request) {
            Auth::logout();
            return redirect('/admin/login');
          }
    


} 