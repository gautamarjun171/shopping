<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\DB;

use App\User;
use App\Model\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;               

class CategoryController extends Controller
{

    public function getcategory(){
        return view('AdminLte.pages.category.category');
    }

    public function addcategory(Request $request ) {
        $obj = new category;
        $obj->title = $request->title;
        $obj->parentId = '0';
        $obj->metatitle = $request->title;
        $obj->slug =   str_replace(' ', '', $request->title);
        $obj->content = $request->content;
        $obj->save();
        if($obj->save() == 1 ) {
            return back()->with('success','Category is created successfully!');
      }else {
            return redirect()->route('admin/category')
              ->with('error','You have no permission for this page!');
      }
    }

    public function getcategoryList() {
        $category = Category::where('parentId','=','0')->get();
        return view('AdminLte.pages.Category.category-list',['categories'=>$category]);
       
    }

    public function editcategory($id) {
        $category = category::find($id);
        return view('AdminLte.pages.Category.category-edit',['category'=>$category]);
        
    }
    public function deletecategory($id) {
        $flight = category::find($id);

        if($flight->delete() == 1 ) {
            return back()->with('success','Category is deleted successfully!');
      }else {
            return redirect()->route('admin/category')
              ->with('error','You have no permission for this page!');
      }
   }
}

 
