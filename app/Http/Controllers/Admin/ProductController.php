<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\DB;

use App\User;
use App\Model\Product;
use App\Model\Images;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;               

class ProductController extends Controller
{
    public function AddProduct(Request $request) {

        $request->validate([
        'img' => 'required|image|mimes:jpeg,png,jpg,gif',
    ]);

    $imageName = time().'.'.$request->img->extension();  
    
    $request->img->move(public_path('images'), $imageName);

       $obj = new Product;
       $obj->title = $request->title;
       $obj->userId = Auth::user()->id;
       $obj->metatitle = $request->title;
       $obj->summary = $request->summary;
       $obj->type = '1';
       $obj->sku = 'test';
       $obj->content = $request->summary;
       $obj->slug = str_replace(' ', '', $request->title);
       $obj->price = $request->price;
       $obj->discount = $request->discount;
       $obj->quantity = $request->quantity;
       $obj->shop = '1';
       $obj->published_at = $request->published_at; 
       $obj->start_at = $request->start_at; 
       $obj->end_at = $request->end_at;
       if($obj->save() == 1 ) {

            $proimg =  new Images;
            $proimg->productId = $obj->id; 
            $proimg->path = 'images/' . $imageName;
            $proimg->save();
            return back()->with('success','Product is added successfully!');
      }else {
             return redirect()->route('admin/product')
               ->with('error','You have no permission for this page!');
       }
    }


    public function getProductList() {
        $product = Product::all();
        return view('AdminLte.pages.product.product-list',['products'=>$product]);
    }

    public function editProduct($id) {
        $product = product::find($id);
        return view('AdminLte.pages.product.product-edit',['product'=>$product]);
    }


    public function deleteProduct($id) {
        $flight = product::find($id);

        if($flight->delete() == 1 ) {
            return back()->with('success','Product is deleted successfully!');
      }else {
            return redirect()->route('admin/product')
              ->with('error','You have no permission for this page!');
      }
   }


   public function updateProduct(Request $request) {

    $obj = Product::find($request->id);
    $obj->title = $request->title;
    $obj->userId = Auth::user()->id;
    $obj->metatitle = $request->title;
    $obj->summary = $request->summary;
    $obj->type = '1';
    $obj->sku = 'test';
    $obj->content = $request->summary;
    $obj->slug = $request->slug;
    $obj->price = $request->price;
    $obj->discount = $request->discount;
    $obj->quantity = $request->quantity;
    $obj->shop = '1';
    $obj->start_at = $request->start_at; 
    $obj->end_at = $request->end_at;
    $obj->save();
        if($obj->save() == 1 ) {
            return back()->with('success','Product is updated successfully!');
        }else {
            return redirect()->route('admin/product')
                ->with('error','You have no permission for this page!');
        }
    }
}