<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\DB;

use App\User;
use App\Model\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;               

class SubCategoryController extends Controller
{

    public function getsubcategory(){
        $category = Category::where('parentId','=','0')->get();
        return view('AdminLte.pages.subcategory.subcategory',['category'=>$category]);
    }

    public function addSubCategory(Request $request ) {

        $obj = new category;
        $obj->title = $request->title;
        $obj->parentId = $request->maincat;
        $obj->metatitle = $request->title;
        $obj->slug = $request->slug;
        $obj->content = $request->content;
        $obj->save();
        if($obj->save() == 1 ) {
            return back()->with('success','SubCategory is created successfully!');
      }else {
            return redirect()->route('admin/subcategory')
              ->with('error','You have no permission for this page!');
      }
    }

    public function getSubCategoryList() {
        $category = Category::where('parentId','!=','0')->get();
        return view('AdminLte.pages.Subcategory.subcategory-list',['categories'=>$category]);
       
    }

    public function editcategory($id) {
        $category = category::find($id);
        return view('AdminLte.pages.Category.category-edit',['category'=>$category]);        
    }


    public function deletecategory($id) {
        $flight = category::find($id);

        if($flight->delete() == 1 ) {
            return back()->with('success','Category is deleted successfully!');
      }else {
            return redirect()->route('admin/category')
              ->with('error','You have no permission for this page!');
      }
   }

   public function postSubCategory( Request $request) {
    $submenu = Category::where('parentId','=',$request->main_menu_id)->get()->toArray();

    return  json_encode(
         $submenu
    );

}


}

 
