<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Model\Product;
use App\Model\Cart;
use App\Model\CartItem;
use Auth;
use Session;
use Illuminate\Support\Facades\DB;


class CartController extends Controller {

    public function getcart() {

        $cartinfo = DB::table('cart')
//        ->select('product.*','images.path')
        ->orderBy('cart.id', 'asc')
        ->leftJoin('cart_item', 'cart.id', '=', 'cart_item.cartId')
        ->get();

//        dd($cartinfo);
        return view('pages.cart',['cartinfo'=>$cartinfo]);
    }

    public function postCart(Request $request) {

        if(Auth::user() === null) {
            return redirect('/auth/login');
        }else {
            $product= Product::find($request->product_id);
            $cart = new Cart;
            $cart->userId = Auth::user()->id;
            $cart->token = $request->_token;
            $cart->sessionId = Session::getId();
            $cart->status = '1';
            $cart->firstName = Auth::user()->firstname;
            $cart->middleName = Auth::user()->middlename;
            $cart->lastName = Auth::user()->lastname;
            $cart->mobile = Auth::user()->mobile;
            $cart->email = Auth::user()->email;
            // $cart->line1 = Auth::user()->line1;
            // $cart->city = Auth::user()->city;
            // $cart->province = Auth::user()->province;
            // $cart->country = Auth::user()->country;
            $cart->content = Auth::user()->content;
            if($cart->save()) {
    
              $cartItem =   new CartItem;
              $cartItem->productId = $request->product_id;
              $cartItem->cartId = $cart->id;
              $cartItem->price = $product->price;
              $cartItem->discount = $product->discount;
              $cartItem->quantity = $product->quantity;
              $cartItem->active = '1';
              $cartItem->content = $product->content;
              if($cartItem->save()) {
                return back()->with('success','Product is created successfully!');
              } else {
                    return redirect()->route('product/cart')
                    ->with('error','You have no permission for this page!');
                }
            }
    
        }  
    }

}
    