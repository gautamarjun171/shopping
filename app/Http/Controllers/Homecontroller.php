<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Model\Product;
use App\Model\CartItem;
use App\Model\Category;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller {


    public function getindex() {

        $products = DB::table('product')
                ->select('product.*','images.path')
                ->orderBy('product.id', 'asc')
                ->leftJoin('images', 'product.id', '=', 'images.productId')
                ->get();

        return view('pages.home', ['products'=>$products]);
    }

    public function getelectronics() {
        return view('pages.electronics');
    }

    public function getproduct() {
        return view('pages.product');
    }
    
    public function getCategory() {
        return view('pages.category');
    }

    // public function getCategoryProduct() {
    //     return view('pages.catagoery');
    // }
}
     