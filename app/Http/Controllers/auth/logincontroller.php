<?php

namespace App\Http\Controllers\Auth;

use App\User;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

//    use AuthenticatesUsers;

//    use LoginUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function getlogin(){
        return view ('auth.login');
    }

    public function postlogin(Request $request){
        //dd($request->email);


        if ((Auth::attempt(['email' => $request->email, 'password' => $request->password]))){
        // if authentification passed
//        dd(Auth::user());
            return redirect()->intended('/');
        }
        // if unsuccessful -> redirect back
            return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors([
            'approve' => 'Wrong credentials or this account not approved yet.',
       ]);

    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/auth/login');
      }


     
}