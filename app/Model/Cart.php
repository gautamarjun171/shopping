<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cart';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId', 'sessionId', 'token'. 'status','firstName','middleName','lastName','mobile','email','line1','	city','province','country','content' 
    ];

}
