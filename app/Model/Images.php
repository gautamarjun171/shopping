<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id','path'
    ];

}
