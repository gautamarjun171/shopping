<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'metatitle', 'slug','summary','type','price','sku','discount','quantity','shop','published_at','start_at','end_at','content'
    ];

    public function image()
    {
        return $this->hasOne('App\Model\Images');
    }

}
