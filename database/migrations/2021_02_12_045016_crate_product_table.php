<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->foreignId('userId');
            $table->string('title', 75);
            $table->string('metatitle', 100);
            $table->string('slug',100);
            $table->text('summary',100);
            $table->smallInteger('type');
            $table->float('price');
            $table->string('sku', 100);
            $table->float('discount');
            $table->smallInteger('quantity');
            $table->tinyinteger('shop');
            $table->date('published_at');
            $table->date('start_at');
            $table->date('end_at');
            $table->text('content');
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }


}
