<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('userId');
            $table->string('sessionId',100);
            $table->string('sku',100);
            $table->string('token',100);
            $table->smallInteger('status');
            $table->float('subTotal', 8, 2);
            $table->float('itemDiscount', 8, 2);
            $table->float('tax', 8, 2);
            $table->float('shipping', 8, 2);
            $table->float('total', 8, 2);

            $table->string('promo',50);
            $table->float('discount', 8, 2);
            $table->float('grandTotal', 8, 2);
            $table->string('firstName',50);

            $table->string('mobile',15);
            $table->string('email',50);
            $table->string('line1',50);
            $table->string('line2',50);
            $table->string('city',50);
            $table->string('province',50);
            $table->string('country',50);
            $table->text('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
