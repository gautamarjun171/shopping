<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('userId');
            $table->string('sessionId',100);
            $table->string('token',100)->nullable();
            $table->smallInteger('status')->nullable();
            $table->string('firstName',50)->nullable();
            $table->string('middleName',50)->nullable();
            $table->string('lastName',50)->nullable();

            $table->string('mobile',15)->nullable();
            $table->string('email',50)->nullable();
            $table->string('line1',50)->nullable();
            $table->string('line2',50)->nullable();
            $table->string('city',50)->nullable();
            $table->string('province',50)->nullable();
            $table->string('country',50)->nullable();
            $table->text('content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
