<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            'parentId' => 0,
            'title' => 'Electronic',
            'metatitle' => 'Electronic',
        ]);
    }

//     INSERT INTO `category` (`id`, `parentId`, `title`, `metatitle`, `slug`, `content`, `created_at`, `updated_at`) VALUES
// (2, 0, 'Electronic', 'Electronic', 'Electronic', 'this is for electronic category', '2021-05-02 21:46:35', '2021-05-02 21:46:35'),
// (3, 0, 'Fashion', 'Fashion', 'Fashion', 'this is for fashion category', '2021-05-02 21:47:07', '2021-05-02 21:47:07'),
// (4, 0, 'Books', 'Books', 'Books', 'this is for books', '2021-05-02 21:47:27', '2021-05-02 21:47:27'),
// (5, 2, 'Computers', 'Computers', 'computer', 'this is for computer only', '2021-05-02 21:50:00', '2021-05-02 21:50:00'),
// (6, 2, 'Smartphones', 'Smartphones', 'smartphone', 'this is smartphone sub category', '2021-05-02 21:50:44', '2021-05-02 21:50:44'),
// (7, 3, 'Men Clothes', 'Men Clothes', 'men_clothes', 'this is for men clothes category', '2021-05-02 21:51:29', '2021-05-02 21:51:29'),
// (8, 3, 'Women clothes', 'Women clothes', 'women_clothes', 'this is for women clothes', '2021-05-02 21:52:04', '2021-05-02 21:52:04'),
// (9, 4, 'Fiction', 'Fiction', 'fiction', 'this is for books', '2021-05-02 21:52:58', '2021-05-02 21:52:58'),
// (10, 4, 'Adventure', 'Adventure', 'adventure', 'this is for adventure books sub category', '2021-05-02 21:53:31', '2021-05-02 21:53:31');
}
