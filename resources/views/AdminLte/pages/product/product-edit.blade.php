@extends('AdminLte.layouts.default')
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Product</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Product</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> Edit Product </h3>
              </div>

              @include('AdminLte.includes.flash')

              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post" action="{{ url('admin\update\product') }}">

              {{ csrf_field() }}

              <input type="hidden" name="id" value="{{ $product->id  }}">

                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" value="{{ $product->title }}" id="title" placeholder="Enter title">
                  </div>
                  <div class="form-group">
                    <label for="slug">Slug</label>
                    <input type="text" name="slug" class="form-control" id="slug" value="{{ $product->slug }}" placeholder="Enter slug">
                  </div>
                  <div class="form-group">
                    <label for="price">Price</label>
                    <input type="number" name="price" class="form-control" id="price" value="{{ $product->price }}" placeholder="Enter price">
                  </div>
                  <div class="form-group">
                    <label for="summary">summary</label>
                    <textarea name="summary"  class="form-control"> {{ $product->summary }}</textarea>
                  </div>
                  <div class="form-group">
                    <label for="discount">Discount ( % )</label>
                    <input type="number" name="discount" class="form-control" value="{{ $product->discount }}" id="discount" placeholder="Enter discount">
                  </div>
                  <div class="form-group">
                    <label for="quantity">Quantity (between 1 and 500):</label>
                    <input type="number" id="quantity" name="quantity" min="1" max="500" value="{{ $product->quantity }}" class="form-control" id="quantity" placeholder="Enter quantity">
                  </div>
                  <div class="form-group">
                    <label for="shop">Shop</label>
                    <input type="text" name="shop" class="form-control" id="shop" value="{{ $product->shop }}" placeholder="Enter shop">
                  </div>
                  <div class="form-group">
                    <label for="start_at">Start at</label>
                    <input type="date" name="start_at" class="form-control" id="start_at" value="2021-05-01" placeholder="Enter Start at date and time">
                  </div>
                  <div class="form-group">
                    <label for="end_at">End at</label>
                    <input type="date" name="end_at" class="form-control" id="end_at" value="{{ $product->end_at }}" placeholder="Enter end at date and time">
                  </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@stop

