@extends('AdminLte.layouts.default')
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Product</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Product</li>
            </ol>
            @include('AdminLte.includes.flash')

          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


    <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"> <a href="{{ url('admin/product') }}"> Add Product </a> </h3>



                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 300px;">
                <table class="table table-head-fixed text-nowrap">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>User</th>
                      <th>Title</th>
                      <th>Category</th>
                      <th>Quantity</th>
                      <th>price</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                
                     @foreach($products as $product)
                    <tr>
                      <td> {{ $product->id }}</td>
                      <td> {{ $product->userId }}</td>
                      <td> {{ $product->title }}</td>
                      <td>  </td>
                      <td> {{ $product->quantity }}</td>
                      <td> {{ $product->price }}</td>
                      <td>
                           <a href="{{ url('admin/product/edit/'. $product->id) }}" alt="edit form"><i class="fas fa-edit"> </i></a> 
                          <a href="{{ url('admin/product/delete/' . $product->id) }}" alt="delete form" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fas fa-trash-alt"></i></a>  
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->




    @stop
