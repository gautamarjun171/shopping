@extends('AdminLte.layouts.default')
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Product</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Product</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> Product </h3>
              </div>

              @include('AdminLte.includes.flash')

              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="{{ url('admin\add\product') }}" enctype="multipart/form-data" >

              {{ csrf_field() }}

                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" id="title" placeholder="Enter title">
                  </div>

                  <div class="form-group">
                    <label for="title">Choose Category</label>
                    <select name="cat" id="mainmenu" class="form-control">
                    <?php  foreach($maincat as $main){ ?>
                      <option value="{{ $main->id }}" >{{ $main->title }}</option>
                    <?php } ?>  
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="title">Choose Sub Category</label>
                    <select id="submenu" class="form-control">
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="price">Price</label>
                    <input type="number" name="price" class="form-control" id="price" placeholder="Enter price">
                  </div>
                  <div class="form-group">
                    <label for="price">Product Image</label>
                    <input type="file" name="img" class="form-control" >
                  </div>

                  <div class="form-group">
                    <label for="summary">summary</label>
                    <textarea name="summary" class="form-control"></textarea>
                  </div>
                  <div class="form-group">
                    <label for="discount">Discount ( % )</label>
                    <input type="number" name="discount" class="form-control" id="discount" placeholder="Enter discount">
                  </div>
                  <div class="form-group">
                    <label for="quantity">Quantity (between 1 and 500):</label>
                    <input type="number" id="quantity" name="quantity" min="1" max="500" class="form-control" id="quantity" placeholder="Enter quantity">
                  </div>
                  <div class="form-group">
                    <label for="shop">Shop</label>
                    <input type="text" name="shop" class="form-control" id="shop" placeholder="Enter shop">
                  </div>
                  <div class="form-group">
                    <label for="published_at">Published at</label>
                    <input type="date" name="published_at" class="form-control" id="published_at" placeholder="Enter published at date and time">
                  </div>
                  <div class="form-group">
                    <label for="start_at">Start at</label>
                    <input type="date" name="start_at" class="form-control" id="start_at" placeholder="Enter Start at date and time">
                  </div>
                  <div class="form-group">
                    <label for="end_at">End at</label>
                    <input type="date" name="end_at" class="form-control" id="end_at" placeholder="Enter end at date and time">
                  </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>

  $(document).ready(function(){

    $('#mainmenu').change(function(){
         var main_menu_id = $('#mainmenu').val();

         $('#submenu').append('<option>' +  + '</option>');     
    $.ajax({
         type: 'POST',
         url: 'product/subcategory',
         data: {'_token':'{{csrf_token()}}',"main_menu_id": main_menu_id},
         success: function (data) {

          var submenus = JSON.parse(data);
              for(var i=0; i<submenus.length; i++){
                console.log(submenus[i]);
              $('#submenu').append('<option value="' +submenus[i]['id'] + '">'+ submenus[i]['title'] +'</option>');
            }   
          },
         error: function () {
             alert('what ever');
         }
     }); 

        });

      });
</script>


