@extends('AdminLte.layouts.default')
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Category</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Category</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> Edit Category </h3>
              </div>

              @include('AdminLte.includes.flash')

              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post" action="{{ url('admin\update\Category') }}">

              {{ csrf_field() }}

              <input type="hidden" name="id" value="{{ $category->id  }}">

                <div class="card-body">
                <div class="form-group">
                    <label for="id">Id</label>
                    <input type="text" name="id" class="form-control" value="{{ $category->id }}" id="id" placeholder="Enter id">
                  </div>
                  <div class="form-group">
                    <label for="parentId">ParentId</label>
                    <input type="text" name="parentId" class="form-control" value="{{ $category->parentId }}" id="parentId" placeholder="Enter parentId">
                  </div>
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" value="{{ $category->title }}" id="title" placeholder="Enter title">
                  </div>
                  <div class="form-group">
                  <label for="metatitle">Metatitle</label>
                    <input type="text" name="metatitle" class="form-control" value="{{ $category->metatitle }}" id="metatitle" placeholder="Enter metatitle">
                  </div>
                  <div class="form-group">
                    <label for="slug">Slug</label>
                    <input type="text" name="slug" class="form-control" id="slug" value="{{ $category->slug }}" placeholder="Enter slug">
                  </div>
                  <div class="form-group">
                    <label for="content">Content</label>
                    <textarea name="content"  class="form-control"> {{ $category->content }}</textarea>
                  </div>
                 </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@stop

