@extends('AdminLte.layouts.default')
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>SubCategory</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Category</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> SubCategory </h3>
              </div>

              @include('AdminLte.includes.flash')

              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="{{ url('admin/add/subcategory') }}">

              {{ csrf_field() }}

              <div class="card-body">
                  
                  <div class="form-group">
                   <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" id="title" placeholder="Enter title">
                  </div>

                  <div class="form-group">
                    <label for="title">Choose Main Category</label>
                    <select name="maincat" id="cat" class="form-control">
                    <?php  foreach($category as $main){ ?>
                      <option value="{{ $main->id }}">{{ $main->title }}</option>
                    <?php } ?>  
                    </select>
                    </div>
                
                  <div class="form-group">
                   <div class="form-group">
                    <label for="title">Slug</label>
                    <input type="text" name="slug" class="form-control" id="title" placeholder="Enter Slug">
                  </div>

                  <div class="form-group">
                    <label for="content">Content</label>
                    <textarea name="content" class="form-control"></textarea>
                  </div>
                  
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


@stop

