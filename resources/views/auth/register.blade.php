<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>E-Commerce Template</title>
    <link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">

    <link href="//fonts.googleapis.com/css?family=Righteous" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300i,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i" rel="stylesheet">

    <link rel="stylesheet" href="{{ URL::asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/css/style.css') }}">
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

    <script>

    $(document).ready(function(){

        $('#submit').click(function(){
            validateForm();   
        });

    })

    function validateForm(){
       // alert('her');
        $('#myform').validate({ // initialize the plugin
            rules: {
                name: {
                    required: true,
                    minlength: 5
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                },
                c_password: {
                    required: true,
                },
            },
            messages: {
                name: "Please enter your name !",
                email: {
                required: "We need your email address",
                email: "Your email address must be in the format of name@domain.com"
                },
                password: "Please enter your Password !",
                c_password: "Please enter confirm Password !",

            }
        });
    }



   </script>
</head>
<body>
    <div class="container-fluid">
        <div class="row min-vh-100">
        
            
            <div class="col-12">
            <header class="row">
                    <!-- Top Nav -->
                    <div class="col-12 bg-dark py-2 d-md-block d-none">
                        <div class="row">
                            <div class="col-auto mr-auto">
                                <ul class="top-nav">
                                    <li>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-auto">
                                <ul class="top-nav">
                                    <li>
                                        <a href="register"><i class="fas fa-user-edit mr-2"></i>Register</a>
                                    </li>
                                    <li>
                                        <a href="login"><i class="fas fa-sign-in-alt mr-2"></i>Login</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Top Nav -->

                    <!-- Header -->
                    <div class="col-12 bg-white pt-4">
                        <div class="row">
                            <div class="col-lg-auto">
                                <div class="site-logo text-center text-lg-left">
                                    
                                </div>
                            </div>
                            <div class="col-lg-5 mx-auto mt-4 mt-lg-0">
                             
                            </div>
                            <div class="col-lg-auto text-center text-lg-left header-item-holder">
                                <a href="#" class="header-item">
                                    
                                    
                            </a></div><a href="#" class="header-item">
                        </a></div><a href="#" class="header-item">

                        <!-- Nav -->
                        </a><div class="row"><a href="#" class="header-item">
                            </a><nav class="navbar navbar-expand-lg navbar-light bg-white col-12"><a href="#" class="header-item">
                                <button class="navbar-toggler d-lg-none border-0" type="button" data-toggle="collapse" data-target="#mainNav">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                </a><div class="collapse navbar-collapse" id="mainNav"><a href="#" class="header-item">
                                    </a><ul class="navbar-nav mx-auto mt-2 mt-lg-0"><a href="#" class="header-item">
                                        </a><li class="nav-item active"><a href="#" class="header-item">
                                            </a><a class="nav-link" href="index.html"> <span class="sr-only">(current)</span></a>
                                        </li>
                                        <li class="nav-item dropdown">
                                        </li>
                                        <li class="nav-item dropdown">
                                            
                              
                        <!-- Nav -->

                    </li></ul></div>
                    <!-- Header -->

                </nav></div></div></header>
           
           
                <!-- Main Content -->
           
                <div class="row">
                    <div class="col-12 mt-3 text-center text-uppercase">
                        <h2>Register</h2>
                    </div>
                </div>

                <main class="row">
                    <div class="col-lg-4 col-md-6 col-sm-8 mx-auto bg-white py-3 mb-4">
                        <div class="row">
                            <div class="col-12">
                                <form id="myform" method="post" action="{{ url('auth/signup') }}">
                                {{ csrf_field() }}

                                    <div class="form-group">
                                        <input type="text" placeholder="Name" id="name" name="name" class="form-control" >
                                    </div>
                                    <div class="form-group">
                                        <input type="email" id="email" placeholder="Email" name="email" class="form-control" >
                                    </div>
                                    <div class="form-group">
                                        <input type="password" id="password" placeholder="Password" name="password" class="form-control" >
                                    </div>
                                    <div class="form-group">
                                        <input type="password" id="password-confirm" placeholder="Confirm Password" name="c_password" class="form-control" >
                                    </div>
                                    <!-- <div class="form-group">
                                        <div class="form-check">
                                            <input type="checkbox" id="agree" class="form-check-input" >
                                            <label class="att_name" for="agree" class="form-check-label ml-2">I agree to Terms and Conditions</label>
                                        </div>
                                    </div> -->
                                    <div class="form-group">
                                        <button type="submit" id="submit" class="btn btn-outline-dark">Register</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </main>
                <!-- Main Content -->
            </div>
            
        </div>
    </div>

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
</body>
</html>