<?php 
        $electronic =  \App\Model\Category::where('parentId', 2)->get();
        $fashion =  \App\Model\Category::where('parentId', 3)->get();
        $books =  \App\Model\Category::where('parentId', 4)->get();
?>

<header class="row">

    <?php if(!Auth::user()){ ?>
    <!-- Top Nav -->
    <div class="col-12 bg-dark py-2 d-md-block d-none">
        <div class="row">
            <div class="col-auto mr-auto">
                <ul class="top-nav">
                    <li>
                        <a href="tel:+123-456-7890"><i class="fa fa-phone-square mr-2"></i>+123-456-7890</a>
                    </li>
                    <li>
                        <a href="mailto:mail@ecom.com"><i class="fa fa-envelope mr-2"></i>mail@ecom.com</a>
                    </li>
                </ul>
            </div>
            <div class="col-auto">
                <ul class="top-nav">
                    <li>
                        <a href="{{ url('auth/register') }}"><i class="fas fa-user-edit mr-2"></i>Register</a>
                    </li>
                    <li>
                        <a href="{{ url('auth/login') }}"><i class="fas fa-sign-in-alt mr-2"></i>Login</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

<?php }else{ ?>
    <!-- Top Nav -->
    <div class="col-12 bg-dark py-2 d-md-block d-none">
        <div class="row">
            <div class="col-auto mr-auto">
                <ul class="top-nav">
                    <li>
                        <a href="tel:+123-456-7890"><i class="fa fa-phone-square mr-2"></i>+123-456-7890</a>
                    </li>
                    <li>
                        <a href="mailto:mail@ecom.com"><i class="fa fa-envelope mr-2"></i>mail@ecom.com</a>
                    </li>
                </ul>
            </div>
            <div class="col-auto">
                <ul class="top-nav">
                    <li>
                        <a href="#"> <i class="fas fa-user-edit mr-2"></i> <?php echo Auth::user()->name; ?></a>
                    </li>
                    <li>
                        <a href="{{ url('auth/logout') }}"><i class="fas fa-sign-in-alt mr-2"></i>Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?php } ?>
    
    
    <!-- Top Nav -->

    <!-- Header -->
    <div class="col-12 bg-white pt-4">
        <div class="row">
            <div class="col-lg-auto">
                <div class="site-logo text-center text-lg-left">
                    <a href="{{ url('/') }}">E-Commerce</a>
                </div>
            </div>
            <div class="col-lg-5 mx-auto mt-4 mt-lg-0">
                <form action="#">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="search" class="form-control border-dark" placeholder="Search..." required>
                            <div class="input-group-append">
                                <button class="btn btn-outline-dark"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-auto text-center text-lg-left header-item-holder">
                <a href="#" class="header-item">
                    <i class="fas fa-heart mr-2"></i><span id="header-favorite">0</span>
                </a>
                <a href="{{ url('cart') }}" class="header-item">
                    <i class="fas fa-shopping-bag mr-2"></i><span id="header-qty" class="mr-3">2</span>
                    <i class="fas fa-money-bill-wave mr-2"></i><span id="header-price">$4,000</span>
                </a>
            </div>
        </div>

        <!-- Nav -->
        <div class="row">
            <nav class="navbar navbar-expand-lg navbar-light bg-white col-12">
                <button class="navbar-toggler d-lg-none border-0" type="button" data-toggle="collapse" data-target="#mainNav">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="mainNav">
                    <ul class="navbar-nav mx-auto mt-2 mt-lg-0">
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="electronics" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Electronics</a>
                            <div class="dropdown-menu" aria-labelledby="electronics">
                                <?php foreach($electronic as $elec){ ?>
                                    <a class="dropdown-item" href="{{ url('/category') }}"> {{ $elec->title }}  </a>
                                <?php } ?>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="fashion" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Fashion</a>
                            <div class="dropdown-menu" aria-labelledby="fashion">
                            <?php foreach($fashion as $faslist) { ?>
                                    <a class="dropdown-item" href="{{ url('/category') }}"> {{ $faslist->title }}  </a>
                             <?php } ?>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="books" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Books</a>
                            <div class="dropdown-menu" aria-labelledby="books">
                            <?php foreach($books as $book) { ?>
                                    <a class="dropdown-item" href="{{ url('/category') }}"> {{ $book->title }}  </a>
                             <?php } ?>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- Nav -->

    </div>
    <!-- Header -->

</header>
