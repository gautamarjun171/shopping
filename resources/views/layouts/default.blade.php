<!doctype html>
<html lang="en">

@include('includes.head')

<body>
    <div class="container-fluid">

        <div class="row min-vh-100">
            <div class="col-12">

            @include('includes.header')						
            
            </div>

            <div class="col-12">
                <!-- Main Content -->
                
                @yield('content')

                <!-- Main Content -->
            </div>

            <div class="col-12 align-self-end">

            @include('includes.footer')

            </div>
        </div>

    </div>

    <!-- Messages -->
    <!-- <div class="message-holder">
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert">
                <small><i class="fas fa-times"></i></small>
            </button>
            <i class="fas fa-check-circle mr-2"></i>
            This is a success alert message.
        </div>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert">
                <small><i class="fas fa-times"></i></small>
            </button>
            <i class="fas fa-times-circle mr-2"></i>
            This is a error alert message.
        </div>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert">
                <small><i class="fas fa-times"></i></small>
            </button>
            <i class="fas fa-exclamation-circle mr-2"></i>
            This is a warning alert message.
        </div>
        <div class="alert alert-info alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert">
                <small><i class="fas fa-times"></i></small>
            </button>
            <i class="fas fa-info-circle mr-2"></i>
            This is a info alert message.
        </div>
    </div> -->
    <!-- Messages -->

    <script type="text/javascript" src="{{ URL::asset('public/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/js/script.js') }}"></script>
</body>
</html>