<?php //  dd($products[1]); ?>

@extends('layouts.default')
@section('content')


<main class="row">

<!-- Slider -->
<div class="col-12 px-0">
    <div id="slider" class="carousel slide w-100" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#slider" data-slide-to="0" class="active"></li>
            <li data-target="#slider" data-slide-to="1"></li>
            <li data-target="#slider" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <img src="public/images/slider-1.jpg" class="slider-img">
            </div>
            <div class="carousel-item">
                <img src="public/images/slider-2.jpg" class="slider-img">
            </div>
            <div class="carousel-item">
                <img src="public/images/slider-3.jpg" class="slider-img">
            </div>
        </div>
        <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<!-- Slider -->

<!-- Featured Products -->
<div class="col-12">
    <div class="row">
        <div class="col-12 py-3">
            <div class="row">
                <div class="col-12 text-center text-uppercase">
                    <h2>Featured Products</h2>
                </div>
            </div>
            <div class="row">
<?php   foreach($products as $product){ ?>

                <!-- Product -->
                <div class="col-lg-3 col-sm-6 my-3">
                <form method="post" enctype="multipart/form-data" action="{{ url('product/cart') }}">
                {{ csrf_field() }}
                <input type="hidden" name="product_id" class="product_id" value="{{ $product->id }}" >
                    <div class="col-12 bg-white text-center h-100 product-item">
                        <div class="row h-100">
                            <div class="col-12 p-0 mb-3">
                                <a href="product.html">
                                    <img src="public/{{ $product->path }}" class="img-fluid">
                                </a>
                            </div>
                            <div class="col-12 mb-3">
                                <a href="product.html" class="product-name"> {{ $product->title }}  </a>
                            </div>
                            <div class="col-12 mb-3">
                                <span class="product-price-old">
                                {{ $product->price }}
                                </span>
                                <br>
                                <span class="product-price">
                               {{ $product->price }}
                                </span>
                            </div>
                            <div class="col-12 mb-3 align-self-end">
                                <button class="btn btn-outline-dark addtocart" type="button"><i class="fas fa-cart-plus mr-2"></i>Add to cart</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <?php  } ?>
                <!-- Product -->

            </div>
        </div>
    </div>
</div>
<!-- Featured Products -->

<div class="col-12">
    <hr>
</div>

<!-- Latest Product -->
<div class="col-12">
    <div class="row">
        <div class="col-12 py-3">
               <div class="col-12 text-center text-uppercase">
                    <h2>Latest Products</h2>
                </div>
            
            <div class="row">
                <!-- Product -->
                <?php  foreach($products as $product){  ?>
                <div class="col-lg-3 col-sm-6 my-3">
                <form method="post"  enctype="multipart/form-data" action="{{ url('product/cart') }}">
                {{ csrf_field() }}

                    <div class="col-12 bg-white text-center h-100 product-item">
                        <span class="new">New</span>
                        <input type="hidden" name="product_id" class="product_id" value="{{ $product->id }}" >
                        <div class="row h-100">
                            <div class="col-12 p-0 mb-3">
                                <a href="product.html">
                                    <img src="public/{{ $product->path }}" class="img-fluid">
                                </a>
                            </div>
                            <div class="col-12 mb-3">
                                <a href="#" class="product-name"> {{ $product->title }} </a>
                            </div>
                            <div class="col-12 mb-3">
                                <span class="product-price-old">
                                {{ $product->price }}
                                </span>
                                <br>
                                <span class="product-price">
                                {{ $product->price }}
                                </span>
                            </div>
                            <div class="col-12 mb-3 align-self-end">
                                <button class="btn btn-outline-dark addtocart" type="submit" ><i class="fas fa-cart-plus mr-2"></i>Add to cart</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <?php } ?>

            </div>
        </div>
    </div>
</div>
<!-- Latest Products -->

<div class="col-12">
    <hr>
</div>

<!-- Top Selling Products -->
<div class="col-12">
    <div class="row">
        <div class="col-12 py-3">
            <div class="row">
                <div class="col-12 text-center text-uppercase">
                    <h2>Top Selling Products</h2>
                </div>
            </div>
            <div class="row">

                <!-- Product -->
                <div class="col-lg-3 col-sm-6 my-3">
                    <div class="col-12 bg-white text-center h-100 product-item">
                        <div class="row h-100">
                            <div class="col-12 p-0 mb-3">
                                <a href="product.html">
                                    <img src="public/images/image-4.jpg" class="img-fluid">
                                </a>
                            </div>
                            <div class="col-12 mb-3">
                                <a href="product.html" class="product-name">Dell Alienware Area 51</a>
                            </div>
                            <div class="col-12 mb-3">
                                <span class="product-price">
                                    $4,500
                                </span>
                            </div>
                            <div class="col-12 mb-3 align-self-end">
                                <button class="btn btn-outline-dark addtocart" type="button"><i class="fas fa-cart-plus mr-2"></i>Add to cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Product -->

            </div>
        </div>
    </div>
</div>
<!-- Top Selling Products -->

<div class="col-12 py-3 bg-light d-sm-block d-none">
    <div class="row">
        <div class="col-lg-3 col ml-auto large-holder">
            <div class="row">
                <div class="col-auto ml-auto large-icon">
                    <i class="fas fa-money-bill"></i>
                </div>
                <div class="col-auto mr-auto large-text">
                    Best Price
                </div>
            </div>
        </div>
        <div class="col-lg-3 col large-holder">
            <div class="row">
                <div class="col-auto ml-auto large-icon">
                    <i class="fas fa-truck-moving"></i>
                </div>
                <div class="col-auto mr-auto large-text">
                    Fast Delivery
                </div>
            </div>
        </div>
        <div class="col-lg-3 col mr-auto large-holder">
            <div class="row">
                <div class="col-auto ml-auto large-icon">
                    <i class="fas fa-check"></i>
                </div>
                <div class="col-auto mr-auto large-text">
                    Genuine Products
                </div>
            </div>
        </div>
    </div>
</div>
</main>

@stop


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>

  $(document).ready(function(){

    $('.addtocart').click(function(){

        var product_id = $('.product_id').val();
        console.log('product_id',product_id);
//        alert(product_id);

    // $.ajax({
    //      type: 'POST',
    //      url: 'product/cart',
    //      data: {'_token':'{{csrf_token()}}',"product_id": product_id},
    //      success: function (data) {

    //       var submenus = JSON.parse(data);
    //           for(var i=0; i<submenus.length; i++){
    //             console.log(submenus[i]);
    //           $('#submenu').append('<option value="' +submenus[i]['id'] + '">'+ submenus[i]['title'] +'</option>');
    //         }   
    //       },
    //      error: function () {
    //          alert('what ever');
    //      }
    //  }); 

        });

      });
</script>
