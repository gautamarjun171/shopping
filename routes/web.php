<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'auth'], function()  
{    
    Route::post('signup','auth\registercontroller@signup');
    // To get the page for the registration
    Route::get('register', 'auth\registercontroller@getregister',);
    //To get the page for the login
    Route::get('login', 'auth\loginController@getlogin');
    Route::post('login', 'auth\loginController@postlogin');
    Route::get('logout', 'auth\loginController@logout');    
});  


Route::group(['prefix' => 'admin'], function()  
{    
    Route::get('/', 'Admin\AdminController@getLogin');    
    Route::get('/login', 'Admin\AdminController@getLogin');    
    Route::post('post/login', 'Admin\Auth\AuthController@postLogin');
    Route::get('logout', 'Admin\Auth\AuthController@logout');   
    Route::get('/dashboard', 'Admin\AdminController@getDashboard');    

    

    Route::get('/product/list', 'Admin\ProductController@getProductList');    
    Route::get('/product', 'Admin\AdminController@getProduct');    
    Route::post('add/product', 'Admin\ProductController@AddProduct');    
    Route::get('/product/edit/{id}', 'Admin\ProductController@editProduct');    
    Route::get('/product/delete/{id}', 'Admin\ProductController@deleteProduct')->name('product.delete');    
    Route::post('update/product', 'Admin\ProductController@updateProduct');    

    Route::get('category','Admin\CategoryController@getcategory');
    Route::post('add/category', 'Admin\CategoryController@addCategory');    
    Route::get('category/list', 'Admin\CategoryController@getCategoryList');    
    Route::get('/category/edit/{id}', 'Admin\CategoryController@editCategory');  
    Route::get('/category/delete/{id}', 'Admin\CategoryController@deleteCategory')->name('category.delete');     

    Route::get('subcategory','Admin\SubCategoryController@getsubcategory');
    Route::post('add/subcategory', 'Admin\SubCategoryController@addSubCategory');    
    Route::get('subcategory/list', 'Admin\SubCategoryController@getSubCategoryList');
    Route::post('product/subcategory', 'Admin\SubCategoryController@postSubCategory');    

    // Route::get('/category/edit/{id}', 'Admin\CategoryController@editCategory');  
    // Route::get('/category/delete/{id}', 'Admin\CategoryController@deleteCategory')->name('category.delete');     


});  


Route::get('/','HomeController@getindex');
Route::get('category','Homecontroller@getCategory');
Route::get('product','Homecontroller@getproduct');
Route::get('cart','CartController@getcart');
Route::post('product/cart', 'CartController@postCart');    
Route::get('category/product','Homecontroller@getCategoryProduct');

